import { PropType, FileType } from 'component-types';

export default {
  props: {
    chapters: {
      label: {
        eng: 'approvalNumber',
      },
      actualType: PropType.String,
    },
  },
};
