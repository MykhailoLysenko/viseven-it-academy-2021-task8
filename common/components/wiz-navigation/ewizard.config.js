import { PropType, FileType } from 'component-types';

export default {
  props: {
    chapters: {
      label: {
        eng: 'Chapters',
      },
      actualType: PropType.Object,
    },
  },
};
