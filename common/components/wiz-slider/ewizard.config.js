import { PropType, FileType } from 'component-types';

export default {
  props: {
    chapters: {
      label: {
        eng: 'sliderProps',
      },
      actualType: PropType.Object,
    },
  },
};
