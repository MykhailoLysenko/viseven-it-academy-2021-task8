import wizText from 'wiz-text';
import wizImage from 'wiz-image';
import wizButton from 'wiz-button';
import wizBlock from 'wiz-block';
import modulePlaceholder from '@blocks/module-placeholder';
import wizSitemap from 'wiz-sitemap'

export default (Vue) => {
  Vue.component('wiz-text', wizText);
  Vue.component('wiz-image', wizImage);
  Vue.component('wiz-button', wizButton);
  Vue.component('wiz-block', wizBlock);
  Vue.component('module-placeholder', modulePlaceholder);
  Vue.component('wiz-sitemap', wizSitemap)
};
